using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Text.Json;
using OpenQA.Selenium;

namespace Mail.Infrastructure
{
    public static class ICookieJarExtensions
    {
        public static void ExportToJson(this ICookieJar jar, string path)
        {
            File.WriteAllText(path, JsonSerializer.Serialize<ReadOnlyCollection<Cookie>>(jar.AllCookies));
        }

        public static void ImportFromJson(this ICookieJar jar, string path)
        {
            JsonSerializer.Deserialize<List<CookieDto>>(File.ReadAllText(path))
                .ForEach(c => jar.AddCookie(new Cookie(c.Name, c.Value, c.Domain, c.Path, c.Expiry))); 
        }
    }

    class CookieDto
    {
        public string Name { get; set; }
        public string Value { get; set;}
        public string Domain { get; set; }
        public string Path { get; set; }
        public DateTime? Expiry { get; set; }
    }
}