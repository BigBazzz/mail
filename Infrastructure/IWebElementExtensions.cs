using OpenQA.Selenium;
using System.Collections.Generic;

namespace Mail.Infrastructure
{
    public static class IWebElementExtensions
    {      
        public static void SendKeysForeach(this IWebElement element, IEnumerable<string> collection, string separator = null)
        {
            if(collection == null) return;

            foreach(var keys in collection)
            {
                element.SendKeys(separator == null ? keys : keys + separator);
            }
        }

        public static IWebElement FindElementOrDefault(this IWebElement element, By by)
        {
            var list = element.FindElements(by);
            
            return list.Count > 0 ? list[0] : null;
        }
    }
}