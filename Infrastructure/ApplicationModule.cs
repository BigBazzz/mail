using System;
using Ninject.Modules;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace Mail.Infrastructure
{
    public class ApplicationModule : NinjectModule
    {
        static string ResourcesPath { get; } = "./Resources";
        public static string JQueryFile { get; } = ResourcesPath + "/jquery-3.5.1.slim.min.js";
        public static string JQueryExtensionFile { get; } = ResourcesPath + "/drag_and_drop_helper.js";
        public static string CookiesFile { get; } = ResourcesPath + "/Cookies.json";

        BrowserName browserType;

        public ApplicationModule(BrowserName browserType) => this.browserType = browserType;
        
        public override void Load()
        {
            IWebDriver driver = null;
            switch(browserType)
            {
                case BrowserName.Firefox:
                    driver = new FirefoxDriver(ResourcesPath);
                    break;
            }

            Bind<IWebDriver>()
                .ToMethod(m =>
                {
                    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
                    return driver;
                })
                .InSingletonScope();
        }
    }

    public enum BrowserName
    {
        Firefox
    }
}