using Ninject;

namespace Mail.Infrastructure
{
    public static class ServiceLocator
    {
        private static IKernel Kernel { get; } = new StandardKernel(new ApplicationModule(BrowserName.Firefox));

        public static T Get<T>()
        {
            return Kernel.Get<T>();
        }

        public static void Dispose()
        {
            Kernel.Dispose();
        }
    }

    public interface IServiceLocator
    {
        T GetService<T>();
    }
}