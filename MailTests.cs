using Mail.Pages;
using NUnit.Framework;
using FluentAssertions;
using Mail.Infrastructure;
using System.Collections.Generic;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace Mail
{
    public class MailTests
    {
        [Order(1), TestCaseSource(nameof(LettersSource))]
        public void SendLetter_New_Success(LetterDto letter)
        {
            var letterPage = new MainPage().CreateLetter();

            letterPage.FillForm(letter).Send();

            letterPage.IsSent.Should().BeTrue();
        }

        [Order(2), TestCaseSource(nameof(LettersSource))]
        public void ReceiveLetter_New_Success(LetterDto letter)
        {
            var letterPage = new MainPage().OpenLetter(letter.Id, FolderType.InBox);
                
            var receivedLetter = letterPage.GetLetterDto();

            receivedLetter.Should().BeEquivalentTo(letter, o => 
                o.ComparingByMembers<LetterDto>().Excluding(l => l.HiddenCopyTo).Excluding(l => l.Message));
        }

        [Order(3), TestCase, Sequential]
        public void MoveLetter_New_Success(
            [Values(1, 1)] int letterId,
            [Values(FolderType.InBox, FolderType.UserFolderFirst)] FolderType from,
            [Values(FolderType.UserFolderFirst, FolderType.Trash)] FolderType to
        )
        {
            var mainPage = new MainPage();
            FolderPageComponent fromFolder = mainPage.SwitchToFolder(from);
            EnvelopePageComponent envelope = mainPage.GetEnvelope(letterId);

            mainPage.DragAndDrop(envelope.Container, mainPage.GetFolder(to).Container);
            
            Thread.Sleep(1000);
            mainPage.Invoking(m => m.GetEnvelope(letterId)).Should().Throw<NoSuchElementException>();
            
            mainPage.SwitchToFolder(to);
            
            mainPage.Invoking(m => m.GetEnvelope(letterId)).Should().NotThrow();
        }

        public static IEnumerable<LetterDto> LettersSource()
        {
            yield return new LetterDto() 
            {
                Id = 1,
                From = "test6845@rambler.ru",
                To = new string[] { "test6845@rambler.ru", "test6845@mailinator.com" },
                CopyTo = new string[] { "test6845@rambler.ru", "test6845@mailinator.com" },
                HiddenCopyTo = new string[] { "test6845@rambler.ru", "test6845@mailinator.com" },
                Subject = "Id:1",
                Message = "Тест: сообщение",
                MessageHtml = "<div>Тест: сообщение<br></div>"
            };
        }
        
        [OneTimeTearDown]
        public void Cleanup()
        {
            Thread.Sleep(1000);
            ServiceLocator.Dispose();
        }
    }
}