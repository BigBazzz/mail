using OpenQA.Selenium;
using Mail.Infrastructure;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using FluentAssertions;
using System.Linq;
using System.IO;

namespace Mail.Pages
{
    public class MainPage : BasePage
    {
        IWebElement FoldersContainer  { get => FindElement(@"div.Sidebar-content-1p"); }
        IWebElement EnvelopesContainer { get => FindElement(@"div.AppContainer-mainContainer-3e"); }

        //Main elements
        IWebElement CreateLetterButton { get => FindElement(@"button[data-list-view=newletter]"); }

        //Folders elements
        public ReadOnlyCollection<IWebElement> FoldersContainers { get => FoldersContainer.FindElements(By.CssSelector("ul._Menu-root-7A>li>div")); }

        //Envelopes elements
        public IWebElement UnseenFilterCheckbox { get => EnvelopesContainer.FindElement(By.CssSelector(@"a.FilterPanel-dummy-1e[data-filters|=unseen]")); }
        public IWebElement FavouriteFilterCheckbox { get => EnvelopesContainer.FindElement(By.CssSelector(@"a.FilterPanel-dummy-1e[data-filters|=favourite]")); }
        public ReadOnlyCollection<IWebElement> EnvelopesContainers { get => EnvelopesContainer.FindElements(By.CssSelector(@"div.MailListItem-dragWrap-2s")); }

        public MainPage() : base(@"body")
        {
            Driver.Navigate().GoToUrl(@"https://mail.rambler.ru/");
            Driver.Manage().Window.Maximize();

            if(!IsAuthorized()) 
            {
                Driver.Manage().Cookies.ImportFromJson(ApplicationModule.CookiesFile);
                Driver.Navigate().Refresh();
            }
        }

        public bool IsAuthorized()
        {
            return Container.FindElementOrDefault(By.CssSelector("[data-cerber-topline=\"user::id\"]")) != null;
        }

        public LetterEditPageComponent CreateLetter()
        {
            CreateLetterButton.Click();

            return new LetterEditPageComponent();
        }

        public LetterReadPageComponent OpenLetter(int Id, FolderType folderType)
        {
            SwitchToFolder(folderType);

            return GetEnvelope(Id).Open();
        }

        public List<FolderPageComponent> GetAllFolders()
        {   
            var folders = new List<FolderPageComponent>();
            FoldersContainers.Where(f => f.FindElementOrDefault(By.CssSelector("a")) != null).ToList()
                .ForEach(f => folders.Add(new FolderPageComponent(f)));
            
            return folders;
        }

        public FolderPageComponent GetFolder(FolderType type)
        {
            var folder = GetAllFolders().Where(f => f.GetFolderType() == type);

            if(folder.Count() == 0) throw new NoSuchElementException($"Папка с именем = {nameof(type)} не найдена.");
            if(folder.Count() > 1) throw new NoSuchElementException($"Найдено более одной папки с именем = {nameof(type)}.");

            return folder.First();
        }

        public FolderPageComponent SwitchToFolder(FolderType folderType)
        {
            var folder = GetAllFolders().Where(f => f.GetFolderType() == folderType).First();
            folder.Open();
            
            return folder;
        }

        public List<EnvelopePageComponent> GetAllEnvelopes()
        {   
            var envelopes = new List<EnvelopePageComponent>();
            EnvelopesContainers.ToList().ForEach(l => envelopes.Add(new EnvelopePageComponent(l)));
            
            return envelopes;
        }

        public EnvelopePageComponent GetEnvelope(int Id)
        {
            var envelope = GetAllEnvelopes().Where(e => e.GetId() == Id);

            if(envelope.Count() == 0) throw new NoSuchElementException($"Соообщение с Id = {Id} не найдено.");
            if(envelope.Count() > 1) throw new NoSuchElementException($"Найдено более одного соообщения с Id = {Id}.");


            return envelope.First();
        }

        public void DragAndDrop(IWebElement source,IWebElement target)
        {
            var js = (IJavaScriptExecutor)Driver;

            var isLibraryLoad = (bool)js.ExecuteScript("return typeof jQuery != 'undefined'");
            if (!isLibraryLoad) js.ExecuteScript(File.ReadAllText(ApplicationModule.JQueryFile));

            var isExtensionLoad = (bool)js.ExecuteScript("return 'simulateDragDrop' in jQuery");
            if (!isLibraryLoad) js.ExecuteScript(File.ReadAllText(ApplicationModule.JQueryExtensionFile));
            
            js.ExecuteScript("$(arguments[0]).simulateDragDrop({ dropTarget: arguments[1]});", source, target);
        }
    }
}