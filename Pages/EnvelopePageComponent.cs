using System;
using System.Linq;
using OpenQA.Selenium;

namespace Mail.Pages
{
    public class EnvelopePageComponent : BasePage 
    {
        public IWebElement SelecteCheckbox { get => FindElement(@"div.MailListItem-checkbox-1q"); }
        public IWebElement FavoriteCheckbox { get => FindElement(@"div.MailListItem-star-Sl"); }
        public IWebElement MarkCheckbox { get => FindElement(@"div.MailListItem-wrapMark-1J"); }
        public IWebElement SubjectText { get => FindElement(@"span.MailListItem-subject-2l"); }
        public IWebElement LetterLink { get => FindElement(@"a.MailListItem-wrapper-3Q"); }

        public EnvelopePageComponent(IWebElement container) : base(container) {}

        public LetterReadPageComponent Open()
        {
            LetterLink.Click();
            return new LetterReadPageComponent();
        }

        public int GetId()
        {
            return Int32.Parse(SubjectText.Text.Split(":").LastOrDefault());
        }

        public void Selecte()
        {
            SelecteCheckbox.Click();
        }
    }
}