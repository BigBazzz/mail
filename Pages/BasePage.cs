using System.Collections.ObjectModel;
using Mail.Infrastructure;
using OpenQA.Selenium;

namespace Mail.Pages
{
    public class BasePage
    {
        protected static IWebDriver Driver { get => ServiceLocator.Get<IWebDriver>(); }
        string cssSelector;
        IWebElement container;
        public IWebElement Container 
        { 
            get => cssSelector == null ? container : Driver.FindElement(By.CssSelector(cssSelector));
            protected set =>  container = value;
        }

        protected BasePage(IWebElement container)
        {
            this.container = container;

        }

        protected BasePage(string cssSelector)
        {
            this.cssSelector = cssSelector;

        }

        protected IWebElement FindElement(string cssSelectorToFind)
        {
            return Container.FindElement(By.CssSelector(cssSelectorToFind));
        }

        protected ReadOnlyCollection<IWebElement> FindElements(string cssSelectorToFind)
        {
            return Container.FindElements(By.CssSelector(cssSelectorToFind));
        }

        protected IWebElement FindElementOrDefault(string cssSelectorToFind)
        {
            return Container.FindElementOrDefault(By.CssSelector(cssSelectorToFind));
        }
    }
}