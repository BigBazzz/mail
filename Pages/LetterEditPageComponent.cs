using Mail.Infrastructure;
using OpenQA.Selenium;
using System.IO;

namespace Mail.Pages
{
    public class LetterEditPageComponent : BasePage
    {
        public IWebElement ToInput { get => FindElement("#receivers"); }
        public IWebElement CopyToInput { get => FindElement("#replicants"); }
        public IWebElement HiddenCopyToInput { get => FindElement("#blindReplicants"); }
        public IWebElement SubjectInput { get => FindElement("#subject"); }
        public IWebElement MessageFrame { get => Driver.FindElement(By.CssSelector("body")); }
        public IWebElement AttachmentInput { get => FindElement("input.Compose-fileInput-dl"); }

        public IWebElement CopyToButton { get => Driver.FindElements(By.CssSelector(".Fields-copyButtonsSwitcher-3X > div"))[0]; }
        public IWebElement HiddenCopyToButton { get => Driver.FindElements(By.CssSelector(".Fields-copyButtonsSwitcher-3X > div"))[1]; }
        public IWebElement SendButton { get => FindElement("button"); }
        public IWebElement SaveToDraftButton { get => FindElements(".Compose-saveToDraftsButton-vP")[0]; }
        public IWebElement RollBackButton { get => FindElements(".Compose-saveToDraftsButton-vP")[1]; }

        public bool IsSent { get; private set; }

        public LetterEditPageComponent() : base(Driver.FindElement(By.CssSelector("#compose"))) {}
        
        public LetterEditPageComponent FillForm(LetterDto letter)
        {
            ToInput.SendKeysForeach(letter.To, "; ");
            
            CopyToButton.Click();
            CopyToInput.SendKeysForeach(letter.CopyTo, "; ");

            HiddenCopyToButton.Click();
            HiddenCopyToInput.SendKeysForeach(letter.HiddenCopyTo, "; ");

            SubjectInput.SendKeys(letter.Subject);
            
            Driver.SwitchTo().Frame("editor_ifr");
            MessageFrame.SendKeys(letter.Message);
            Driver.SwitchTo().DefaultContent();

            AttachmentInput.SendKeysForeach(letter.Attachments);

            return this;
        }

        public void Send()
        {
            SendButton.Click();
            IsSent = Driver.FindElements(By.CssSelector("header.SentLetter-head-1P"))[0].Text.Equals("Письмо отправлено");
        }

        public void SaveToDraft()
        {
            SaveToDraftButton.Click();
        }
    }

    public struct LetterDto
    {
        public int Id { get; set; }
        public string From { get; set; }
        public string[] To { get; set; }
        public string[] CopyTo { get; set; }
        public string[] HiddenCopyTo { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public string MessageHtml  { get; set; }
        public string[] Attachments { get; set; }
    }

    public struct Attachments
    {
        public static string TxtFile { get; } = Path.GetFullPath("./Resources/Document.txt"); 
    }
}