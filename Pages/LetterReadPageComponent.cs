using System.Collections.ObjectModel;
using Mail.Infrastructure;
using OpenQA.Selenium;
using System.Linq;
using System;

namespace Mail.Pages
{
    public class LetterReadPageComponent : BasePage
    {
        public IWebElement FromText { get => FindElement(@"div.LetterHeader-from-kM span.ContactWithDropdown-headerEmail-BC"); }
        public IWebElement AnotherRecipientToogle  { get => Container.FindElementOrDefault(By.CssSelector(@"a.Recipients-toggle-1P[data-test-target=recipients-toggle]")); }
        public ReadOnlyCollection<IWebElement> RecipientsContainer  { get => FindElements(@"div.Recipients-to-3x"); } 
        public ReadOnlyCollection<IWebElement> ToText  { get => RecipientsContainer[0].FindElements(By.CssSelector(@"span.ContactWithDropdown-headerEmail-BC")); }        
        public ReadOnlyCollection<IWebElement> CopyToText { get => RecipientsContainer[1].FindElements(By.CssSelector(@"div.Recipients-to-3x span.ContactWithDropdown-headerEmail-BC")); }
        public IWebElement SubjectText { get => FindElement(@"h2.LetterHeader-subject-3U"); }
        public IWebElement MessageText { get => FindElement(@"#letter div.messageBody>div"); }
        public ReadOnlyCollection<IWebElement> AttachmentsLinks { get => FindElements(@"a.AttachmentsItem-name-Bs"); }

        public LetterReadPageComponent() : base(Driver.FindElement(By.CssSelector(@"div.AppContainer-mainContainer-3e"))) {}
    
        public LetterDto GetLetterDto()
        {
            LetterDto letter = new LetterDto();
            
            letter.Id = GetId();
            letter.From = FromText.Text;

            if(AnotherRecipientToogle != null) AnotherRecipientToogle.Click();
            letter.To = ToText.Select(e => e.Text).ToArray();
            letter.CopyTo = CopyToText.Select(e => e.Text).ToArray();

            letter.Subject = SubjectText.Text;
            letter.MessageHtml = MessageText.GetAttribute("innerHTML");;
            letter.Attachments = AttachmentsLinks.Count > 0 ? AttachmentsLinks.Select(e => e.Text).ToArray() : null;

            return letter;
        }

        public int GetId()
        {
            return Int32.Parse(SubjectText.Text.Split(":").LastOrDefault());
        }
    }
}