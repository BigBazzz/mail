using OpenQA.Selenium;
using System.Linq;
using System;
using System.Web;

namespace Mail.Pages
{
    public class FolderPageComponent : BasePage
    {    
        IWebElement SelecteAllCheckbox { get => FindElement("span.Checkbox-root-vD"); }
        IWebElement DeleteBetton { get => FindElement("div[data-list-view=maillist::trash]"); }

        public FolderPageComponent(IWebElement container) : base(container) {}

        public FolderType GetFolderType()
        {
            return (FolderType)Enum.Parse(
                typeof(FolderType),
                HttpUtility.UrlEncode(
                    Container.FindElement(By.CssSelector("a[href*=\"folder/\"]"))
                    .GetAttribute("href")
                    .Split("/")
                    .LastOrDefault()),
                true
            );
        }

        public void Open()
        {
            Container.Click();
        }
    }

    public enum FolderType
    {
        InBox,
        SentBox,
        DraftBox,
        Trash,
        Spam,
        UserFolderFirst,
        UserFolderSecond
    }
}